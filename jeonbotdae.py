# -*- coding: utf-8 -*-
import re
import urllib.request
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from bs4 import BeautifulSoup
from urllib.parse import quote
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter





app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

def _crawl_clash_chart(text):

    input_info = text.strip().split()
    print(input_info)
    block10 = SectionBlock(text="`@<봇이름> 게임명 닉네임` 과 같이 멘션해주세요.")

    if '롤' in input_info[1] or 'lol' in input_info[1]:
        if len(input_info) == 2:
            return [block10]
        else:
            url = 'https://www.op.gg/summoner/userName=' + quote(str(input_info[2]))

    elif '로얄' in input_info[1] or '클래시로얄' in input_info[1] or '클래시' in input_info[1]:
        if len(input_info) == 2:
            return [block10]
        else:
            url = 'https://cr.op.gg/player/' + quote(str(input_info[2]))
    else:
        return [block10]
    print(url)
    source_code = urllib.request.urlopen(url).read().decode('utf8')
    soup = BeautifulSoup(source_code, "html.parser")
    block11 = SectionBlock(text="'등록되지 않은 플레이어입니다.'")
    for not_tag in soup.find_all("div", class_="no-data__message__text__wrapper"):
        return [block11]

    solo = []
    game_result = []
    recent_result = []

    for score_tag in soup.find_all("div", class_="player-profile__bottom player-trophy"):
        solo.append(score_tag.get_text().strip())

    for result_tag in soup.find_all("div", class_="recent-game-count__WDL"):
        for s_tag in result_tag.find_all("span", class_="recent-game-count__WDL__W"):
            game_result.append(s_tag.get_text().strip())
        for m_tag in result_tag.find_all("span", class_="recent-game-count__WDL__D"):
            game_result.append(m_tag.get_text().strip())
        for p_tag in result_tag.find_all("span", class_="recent-game-count__WDL__L"):
            game_result.append(p_tag.get_text().strip())

    for recent_tag in soup.find_all("ul", class_="recent-game-list__wrapper"):
        for sp_tag in recent_tag.find_all("li"):
            recent_result.append(sp_tag.get_text().strip())

    block0 = ImageBlock(
        image_url="http://post.phinf.naver.net/20160304_75/1457074185685hunWs_PNG/12715551_1658428421088211_1446468016044762881_n.png/IvJh4hGJiAbSY7lK3kpS2bv4z4-8.jpg",
        alt_text="이미지를 로딩중입니다"
    )

    block1 = SectionBlock(
        text="*<" + url + "|" + str(input_info[2]) + ">* "
    )

    block2 = SectionBlock(
        text="`현재 트로피`" + '\n\n' + " :trophy: " + str(solo).replace("[", "").replace("]", "").replace("'", "").replace(
            ",", "")
    )

    last_list = []

    for last_tag1 in soup.find_all("span", class_="player-summary__content__count"):
        last_list.append(last_tag1.get_text().strip().replace(",", ""))

    print(last_list)

    last_text1 = "`최대 트로피`" + "          " + "`시즌 최고 트로피`" + '\n\n' + " :trophy: " + last_list[0]
    count1 = int(17 - len(last_list[0]))
    for i in range(count1):
        last_text1 = last_text1 + " "
    last_text1 = last_text1 + " :trophy: " + last_list[1]

    last_text2 = "`지원 합계`" + "          " + "`매치 합계`" + '\n\n' + ":crossed_swords: " + last_list[2]
    count2 = int(12 - len(last_list[2]))
    for i in range(count2):
        last_text2 = last_text2 + " "
    last_text2 = last_text2 + " :books: " + last_list[3] + '\n\n'

    last_text3 = "`도전최대승리/획득카드`" + '\n\n' + ":fleur_de_lis: " + last_list[4]
    last_text4 = "`토너먼트최대승리/획득카드`" + '\n\n' + ":trident: " + last_list[5]

    block3 = SectionBlock(
        text="`역대 전적`" + '\n\n'
    )

    block4 = SectionBlock(
        fields=[last_text1, last_text2]
    )

    block5 = SectionBlock(
        fields=[last_text3, last_text4]
    )

    result_text = "`최근 승패`" + '\n\n' + game_result[0].replace("W", "승") + " / " + game_result[1].replace("D","무") + " / " + game_result[2].replace("L", "패") + '\n\n'

    count = 0
    for i in recent_result:
        if count == 13:
            result_text = result_text + '\n'
            count = 0
            if i == 'W':
                result_text = result_text + ":o:" + " "
            else:
                result_text = result_text + ":heavy_minus_sign:" + " "
            count += 1

    block6 = SectionBlock(
        text=result_text
    )

    my_blocks = [block0, block1, block2, block3, block4, block5, block6]

    return my_blocks
# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    input_info = text.strip().split()
    #print(input_info)

    block10 = SectionBlock(text="`@<봇이름> 게임명 닉네임` 과 같이 멘션해주세요.")

    if '롤' in input_info[1] or 'lol' in input_info[1]:
        if len(input_info) == 2:
            return [block10]
        else:
            url = 'https://www.op.gg/summoner/userName=' + quote(str(input_info[2]))

    elif '로얄' in input_info[1] or '클래시로얄' in input_info[1] or '클래시' in input_info[1]:
        if len(input_info) == 2:
            return [block10]
        else:
            url = 'https://pubg.op.gg/user/' + quote(str(input_info[2]))
    else:
        return [block10]

    source_code = urllib.request.urlopen(url).read().decode('utf8')
    soup = BeautifulSoup(source_code, "html.parser")

    block5 = SectionBlock(text='등록되지 않은 플레이어입니다')
    for not_tag in soup.find_all("div", class_="SummonerNotFoundLayout"):
        return [block5]

    notFound=soup.find('div','SummonerNotFoundLayout')
    keywords=[]
    ranking=[] #['Ladder Rank 2,668,510 (84% of top)']
    tierset=[] #[['Ranked Solo\nBronze 1\n\n', '\t\t\t41 LP', '\t\t', '\t\t\t\t\t\t/', '\t\t\t\t\t\n3W\n10L\n\nWin Ratio 23%']]
    tier=''    #Ranked solo Bronze 1
    lp=''
    wLRatio=[] #['3W', '10L', 'Win Ratio 23%']

    for span_tag in soup.find_all('div', class_='LadderRank'):
        word = span_tag.get_text().strip()
        ranking.append(word)
    for span_tag in soup.find_all('div', class_='SummonerRatingMedium'):
        word = span_tag.get_text().strip('\n').split('\n\t')
        tierset.append(word)
    # tierImg=soup.find('img', class_='Image') #//opgg-static.akamaized.net/images/site/icon-history-info@2x.png
    # champImg=soup.find('img', class_='ChampionImage')
    # ht = 'https:'
    # tierImg=ht+tierImg.get('src')
    # print(tierImg)
    # champImg=ht+champImg.get('src')

    champname=soup.find('div', class_='ChampionName').get_text().split()[0]
    avRatio = soup.find('div', class_='KDA').get_text().split()[0]  # 1.44:1
    winRatioChamp = soup.find('div', class_='Played').get_text().split()[0]

    for ti in tierset[0]:
        ti=ti.strip('\n\t').split('\n')

    #tierset[0]
    tier=tierset[0][0].split('\n')[1]
    lp=tierset[0][1]
    tierset[0][4]=tierset[0][4].split('\n') #['\t\t\t\t\t', '3W', '10L', '', 'Win Ratio 23%']
    wLRatio.append(tierset[0][4][1])
    wLRatio.append(tierset[0][4][2])
    wLRatio.append(tierset[0][4][4])


    ########출력데이터 가공 ######

    tierLp='*'+tier.strip()+'*'+' '+'*'+lp.strip()+'*'

    wLRatioString = tierLp+'\n'+ '*'+wLRatio[0]+'*'+' '+'*'+wLRatio[1]+'*'+'\n'+'*'+wLRatio[2]+'*'
    champname='*'+champname+'*'+'\n'+'*'+avRatio+'*'+'\n'+'*'+winRatioChamp+'*'
    rankingString=ranking[0].split()
    rankingString='*'+"<https://www.op.gg/summoner/userName=|"+str(input_info[2])+">"       +'                  '+rankingString[2]+'위 '+rankingString[3].strip('(')+'('+rankingString[5]+'*'


    #ranking[0] 순위
    tierString=''
    for i in range(len(tier)):
        tierString +=tier[i]
    tierString=tierString.lower().split()[0]

    print(tierString.lower().split()[0])
   #tierEmoge=tier.lowercase()
    block1 = SectionBlock(fields=["            "+"`tier`","            "+"`most champ`" ])

    #block1 = SectionBlock(fields=[":"+tierEmoge+":"+wLRatioString, "a" ])

    #block2 = ImageBlock(image_url=tierImg, alt_text="이미지가안뜰때보이는문구")
    block2 = ImageBlock(image_url='http://www.lgchallengers.com/wp-content/uploads/2013/08/lol_01.jpg', alt_text="이미지가안뜰때보이는문구")
    #block3 =SectionBlock(fields=[avRatio, ranking[0],champname,winRatioChamp])
    block3 = SectionBlock(fields=[":"+tierString+":"+wLRatioString, champname])
    block4 = SectionBlock(text=rankingString)



    my_blocks = [block2,block4,block1,block3]
    return my_blocks


# def _crawl_rankLol_chart(text):
#
#     input_info = text.strip().split()
#     # print(input_info)
#
#     block10 = SectionBlock(text="`@<봇이름> 게임명 닉네임` 과 같이 멘션해주세요.")
#
#     if '랭크' in input_info[1] or 'rank' in input_info[1]:
#         if len(input_info) == 2:
#             return "`@<봇이름> rank 게임이름` 과 같이 멘션해주세요."
#         elif '롤' in input_info[2] or 'lol' in input_info[2]:
#             url = 'https://www.op.gg/ranking/ladder/'
#
#     source_code = urllib.request.urlopen(url).read().decode('utf8')
#     soup = BeautifulSoup(source_code, "html.parser")
#
#     block5 = SectionBlock(text='등록되지 않은 플레이어입니다')
#     for not_tag in soup.find_all("div", class_="SummonerNotFoundLayout"):
#         return [block5]
#
#     notFound = soup.find('div', 'SummonerNotFoundLayout')
#     keywords = []
#     ranking = []  # ['Ladder Rank 2,668,510 (84% of top)']
#     tierset = []  # [['Ranked Solo\nBronze 1\n\n', '\t\t\t41 LP', '\t\t', '\t\t\t\t\t\t/', '\t\t\t\t\t\n3W\n10L\n\nWin Ratio 23%']]
#     tier = ''  # Ranked solo Bronze 1
#     lp = ''
#     wLRatio = []  # ['3W', '10L', 'Win Ratio 23%']
#
#     for span_tag in soup.find_all('div', class_='LadderRank'):
#         word = span_tag.get_text().strip()
#         ranking.append(word)
#     for span_tag in soup.find_all('div', class_='SummonerRatingMedium'):
#         word = span_tag.get_text().strip('\n').split('\n\t')
#
#
#     # elif '로얄' in input_info[1] or '클래시로얄' in input_info[1] or '클래시' in input_info[1]:
#     #     if len(input_info) == 2:
#     #         return [block10]
#     #     else:
#     #         url = 'https://pubg.op.gg/user/' + quote(str(input_info[2]))
#     # else:
#     #     return [block10]
#
#
#     return my_blocks





# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):

    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    input_info = text.strip().split()
    if '롤' in input_info[1] or 'lol' in input_info[1]:
        my_blocks = _crawl_music_chart(text)

    elif '로얄' in input_info[1] or '클래시로얄' in input_info[1] or '클래시' in input_info[1]:
        my_blocks = _crawl_clash_chart(text)

    # elif '랭크' in input_info[1] or 'rank' in input_info[1] or 'RANK' in input_info[1]:
    #     my_blocks = _crawl_rankLol_chart(text)



    #message = _crawl_music_chart(text)
    #my_blocks = _crawl_music_chart(text)

    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(my_blocks)
        #text=message
    )

@slack_events_adaptor.on("message")
def on_message_received(event_data):
    pass


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8082)
